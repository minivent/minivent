#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <Arduino.h>
#include <avr/wdt.h>

    void onButton_callback();
    void applyButton_callback();
    void open_uSw_callback();
    void enc_callback();

#endif
